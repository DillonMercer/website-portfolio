/*==================== toggle icon navbar ====================*/
let menuIcon = document.querySelector('#menu-icon');
let navbar = document.querySelector('.navbar');

menuIcon.onclick = () => {
    menuIcon.classList.toggle('bx-x');
    navbar.classList.toggle('active');
};


/*==================== scroll sections active link ====================*/
let sections = document.querySelectorAll('section');
let navLinks = document.querySelectorAll('header nav a');

window.onscroll = () => {
    sections.forEach(sec => {
        let top = window.scrollY;
        let offset = sec.offsetTop - 150;
        let height = sec.offsetHeight;
        let id = sec.getAttribute('id');

        if(top >= offset && top < offset + height) {
            navLinks.forEach(links => {
                links.classList.remove('active');
                document.querySelector('header nav a[href*=' + id + ']').classList.add('active');
            });
        };
    });
    /*==================== sticky navbar ====================*/
    let header = document.querySelector('header');

    header.classList.toggle('sticky', window.scrollY > 100);

    /*==================== remove toggle icon and navbar when click navbar link (scroll) ====================*/
    menuIcon.classList.remove('bx-x');
    navbar.classList.remove('active');
};


/*==================== scroll reveal ====================*/
ScrollReveal({
    // reset: true,
    distance: '80px',
    duration: 2000,
    delay: 200
});

ScrollReveal().reveal('.home-content, .heading', { origin: 'top' });
ScrollReveal().reveal('.home-img, .services-container, .portfolio-box, .contact form', { origin: 'bottom' });
ScrollReveal().reveal('.home-content h1, .about-img', { origin: 'left' });
ScrollReveal().reveal('.home-content p, .about-content', { origin: 'right' });


/*==================== typed js ====================*/
const typed = new Typed('.multiple-text', {
    strings: ['Administrator', 'Developer', 'IT Analyst', 'Specialist'],
    typeSpeed: 90,
    backSpeed: 90,
    backDelay: 1000,
    loop: true
});

// dark mode

const colorSwitch = document.getElementById('input-color-switch');

colorSwitch.addEventListener('click',checkMode);

function checkMode(){
    console.log('checking...');
    if(colorSwitch.checked){
        console.log ('dark on');
        darkModeOn();
    }
    else{
        console.log('dark.off')
        darkModeOff();
    }
}
// changing the colors 
function darkModeOn(){
    document.body.classList.add("dark-mode");
    document.documentElement.style.setProperty('--bg-color', '#1f242d');
    document.documentElement.style.setProperty('--second-bg-color', '#323946');
    document.documentElement.style.setProperty('--text-color', '#fff');
}

function darkModeOff(){
    document.body.classList.remove("dark-mode");
    document.documentElement.style.setProperty('--bg-color', '#e7e7e7');
    document.documentElement.style.setProperty('--second-bg-color', '#e2e2e2');
    document.documentElement.style.setProperty('--text-color', '#293E40');
}